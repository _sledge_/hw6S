var mydb = require('./db.js');

var express = require('express');
var app = express();

app.get('/', function (req, res) {res.sendFile(__dirname+'/index.html');});
app.get('/categories', function (req, res) {mydb.loadCategories(req, res);});
app.get('/onecat/:category', function (req, res) {mydb.loadOne(req, res);});
app.get('/onecatanswer/:category', function (req, res) {mydb.loadOneAnswer(req, res);});
//app.get('/questions/:categories', function (req, res) {loadQuestions(req, res);});

app.use(express.static(__dirname + '/'));
var server = app.listen(process.env.PORT, function () {console.log("Server listening at"+ process.env.PORT);});
//var server = app.listen(8081, function () {console.log("Server listening at 8081");});