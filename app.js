function callAdmin() {
    console.log(document.cookie.indexOf('cookie_name=Jeopardy'));
    $("#selectDiv").hide();
    if (document.cookie.indexOf('name=Jeopardy') === -1) { //no cookie set
        $("#login").show();
    }
    else {
        $("#update").show();
    }
    $("#adminDiv").show();
}

function callAuthenticate(user,pass) {
    if (user==='admin' && pass ==='password') {
        document.cookie = "name=Jeopardy";
        $("#login").hide();
        callAdmin();
    }
    else {
        // error message etc.
    }
}

function callGame() {
    var categories = [];
    $("input:checked").each(function() {
        categories.push($(this).attr('name'));
    });

    var players = [];
    $(".players").each(function() {
        if ($(this).val() ) {
            players.push( $(this).val());
        }        
    });

    var url = '/game/categories/players'
    $("#selectDiv").hide();
    $("#gameDiv").show();
    makeGame(categories, players);
}

function makeGame(categories,players) {
    var numCats = categories.length;
    var numPlayers = players.length;

    for(var i = 0; i < numCats; ++i) {
        var cat = document.createElement("div");
        cat.setAttribute('class', "categoryLabel");
        cat.innerHTML = categories[i];
        $('#board').append(cat );
    }
    $(".categoryLabel").css('width',80/numCats+'%');

    $(".box").css('width',80/numCats+'%');
    for(var i = 0; i < 5*numCats; ++i) {
        var question = document.createElement("div");
        question.setAttribute('class', "box");
        question.setAttribute('id', i);
        question.innerHTML = 100 * (1 + i/numCats | 0);

        $('#board').append(question );
    }
    $(".box").css('width',80/numCats+'%');

    for(var i = 0; i < numPlayers; ++i) {
        var playerDiv = document.createElement("div");
        playerDiv.setAttribute('class', "playerDiv");
        playerDiv.setAttribute('score', "0");
        playerDiv.innerHTML = players[i];
        playerDiv.setAttribute('name', players[i]);
        $('#board').append(playerDiv);
    }
    $(".playerDiv").css('width',80/numPlayers+'%');

    makeQuestions(categories);
    makeAnswers(categories);

    $(".box").click(function() {
        $("#question").html(this.getAttribute('question') + '<br><br>' + this.getAttribute('answer'));
        $("#questionDiv").show();
        var id = this.getAttribute('id');
        $("#questionDiv").click(function() {
            $("#questionDiv").hide();
            $( ".playerDiv" ).off("click").click(function() {
                $(this).attr('score', Number($(this).attr('score')) + 100  * (1 + id/numCats | 0));
                $(this).html($(this).attr('name')+$(this).attr('score'))
            })
        })
    })
}

function makeChecks() {
    $.ajax({
        url: "/categories",
        type : "GET",                
        success: function (data) {
            for(var i = 0, len = Object.keys(data).length; i < len; ++i) {   
                var lab = document.createElement("label");
                lab.innerHTML = data[i];
                var check = document.createElement("input");                
                check.type = "checkbox";
                check.name = data[i];
                $('#categories').append(lab);        
                lab.appendChild(check);
            }
                
        }
    });

}


function makeQuestions(cats) {
    for (var i=0, len = cats.length; i < len; ++i) {
        $.ajax({
            url: "/onecat/"+cats[i],
            type : "GET",                
            success: function (data) {
                var els = document.querySelectorAll('.box');
                var len = els.length/5;

                var j = 0;
                while ($(".categoryLabel").eq(j).html()!==data[5]) {++j};

                for(var k=0;k<5;++k) {
                    console.log(j,k,len*k+j);
                    els[len*k+j].setAttribute('question',data[k]);
                }                    
            }            
        })        
    }     
}        

function makeAnswers(cats) {
    for (var i=0, len = cats.length; i < len; ++i) {
        $.ajax({
            url: "/onecatAnswer/"+cats[i],
            type : "GET",                
            success: function (data) {
                var els = document.querySelectorAll('.box');
                var len = els.length/5;

                var j = 0;
                while ($(".categoryLabel").eq(j).html()!==data[5]) {++j};

                for(var k=0;k<5;++k) {
                    console.log(j,k,len*k+j);
                    els[len*k+j].setAttribute('answer',data[k]);
                }                    
            }            
        })        
    }     

}        