var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/JeopardyDb";

exports.loadCategories = function (req, res) { 
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db("JeopardyDb");
      
      dbo.collection("questions").find({}).toArray(function(err, result) {
        db.close();        
        if (err) throw err;
    
        var categories = [];  
        for (var i = 0, len = result.length; i < len; i++) {
          if (categories.includes(i) != true){
            categories.push(result[i].category);
          }
        } 
        categories.sort();
        categories = categories.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
        res.send(categories);  
        });
    }); 
  
  };

exports.loadOne = function(req, res) {
    var cat = req.params.category;
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("JeopardyDb"); 

        dbo.collection("questions").find({category:cat}).toArray(function(err, result) {
            db.close();
            if (err) throw err;

            var questionList = [];
            for (var j = 0, leng = result.length; j < leng; j++) {
                questionList.push(result[j].Question);
            };
            questionList.push(cat);
            res.send(questionList); 
            })        
    })
}

exports.loadOneAnswer = function(req, res) {
    var cat = req.params.category;
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("JeopardyDb"); 

        dbo.collection("questions").find({category:cat}).toArray(function(err, result) {
            db.close();
            if (err) throw err;

            var questionList = [];
            for (var j = 0, leng = result.length; j < leng; j++) {
                questionList.push(result[j].Answere);
            };
            questionList.push(cat);
            res.send(questionList); 
            })        
    })
}